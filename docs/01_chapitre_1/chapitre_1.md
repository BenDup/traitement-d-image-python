---
author: BenDup
title: Chapitre 1 - Contexte
---

## I. Intro

L’objectif n’étant pas de faire en soi du traitement d’images, auquel cas on se contenterait d’utiliser un logiciel dont c’est la finalité. <br>
On va utiliser des programmes réalisés sous python et permettant de réaliser différents traitements AFIN d’analyser le code POUR découvrir la méthode de traitement (et donc pourquoi et comment ça fonctionne, par exemple lorsque vous appliquez un filtre photo sur Instagram 📸).

### 1. Docs fournis

* ce site web avec les scripts à mettre dans Edupython.
* l'image à télécharger à l'accueil.

### 2. Objectifs

1)	Essayer chaque programme sur l’image proposée. <br>
2)	Exprimer le résultat de ce qui est obtenu. <br>
3)	Expliquer à partir du code du programme proposé comment il agit pour parvenir au résultat obtenu.

## II. Déroulé

Au chapitre 2 nous verrons 6 scripts simples, puis 3 plus complexes. <br>
C'est parti pour faire des transformations.
$$
{CuSO_4}_{(s)}   \rightarrow   {Cu^{2+}}_{(aq)}+ {SO_{4}^{2-}}_{(aq)}
$$

Euh non ça c'est une transformation chimique écrite en LaTeX..., mais c'est trop stylé ! 😎

