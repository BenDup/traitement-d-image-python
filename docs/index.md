---
author: BenDup
title: 🏡 Accueil traitement d'image
---

L'objectif est de découvrir différents traitements automatisés d'image sous python. <br>
Vous allez voir différents scripts à copier-coller dans Edupython.


!!! info "Pour que cela fonctionne"

    Après le copié-collé, il faudra d'abord enregistrer votre script (fichier --> enregistrer sous), puis placer l'image de référence dans le MEME dossier ! 
    <br>
    L'image de référence est ici :
    [château-de-chaumont-sur-loire](https://bendup.forge.apps.education.fr/traitement-d-image-python/02_chapitre_2/images/chateau-chaumont.jpg){:target="_blank" }
    
    A enregistrer par clic droit...
    

😊  Bienvenue !




