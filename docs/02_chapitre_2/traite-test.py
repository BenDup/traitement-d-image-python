# --- PYODIDE:env --- #

class Stack:
    """ (Interface à décrire dans l'énoncé) """
    def __init__(self): self.__stk=[]
    def push(self, v): self.__stk.append(v)
    def pop(self): return self.__stk.pop()
    def is_empty(self): return not self.__stk

import micropip
import pyodide
import io
import asyncio
from js import fetch
from pyodide.http import pyfetch
await micropip.install('Pillow')
pyodide.code.eval_code('import PIL')

url_fichier = "chateau-chaumont.jpg"
reponse = await pyfetch(url_fichier)
data = await reponse.bytes()
bytes_list = bytearray(data)
my_bytes = io.BytesIO(bytes_list)


# local ='local.jpg'
# with open(local, 'wb') as f:
#     f.write(data)


# --- PYODIDE:ignore --- #
"""
Code inspiré très fortement de https://community.anaconda.cloud/t/image-uploading-and-manipulation-demo/23586/5
"""
# --- PYODIDE:code --- #

from PIL import Image
from js import document, console, Uint8Array, window, File

im=Image.open(my_bytes)

#on créé une fonction qui effectue la conversion de l’image
def tt(image):
    #on récupère les dimensions de l'image
    (c,l)=image.size

    #on créé une image d'arrivée de même dimensions
    imagearrivee=Image.new('RGB',(c,l))

    #on va parcourir toutes les colonnes
    for x in range (c):
        #pour chaque colonne on va parcourir chaque ligne
        for y in range (l):

            #on récupère le triplet RGB de chaque pixel
            pixel=image.getpixel((x,y))

            #traitement de la couleur du pixel
            p=(0,pixel[1],0)

            #affectation de la couleur du pixel de l'image selon la valeur calculée
            imagearrivee.putpixel((x,y),p)

    # On enregistre dans un flux
    stream = io.BytesIO()
    imagearrivee.save(stream, format='PNG')
    processed_image_file = File.new([Uint8Array.new(stream.getvalue())], "new_image_file.png", {type: "image/png"})
    new_image = document.createElement('img')
    new_image.src = window.URL.createObjectURL(processed_image_file)
    document.getElementById("new_image").appendChild(new_image)

    original_image_file = File.new([Uint8Array.new(my_bytes.getvalue())], "original_image_file.jpg", {type: "image/jpeg"})
    original_image = document.createElement('img')
    original_image.classList.add("w-auto")
    original_image.src = window.URL.createObjectURL(original_image_file)
    document.getElementById("original_image").appendChild(original_image)

#on lance la fonction tt() créée sur notre image
tt(im)
