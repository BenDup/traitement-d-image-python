---
author: BenDup
title: Six scripts assez simples
tags:
  - liste/tableau
---

Comme d'hab dans chaque prog il faudra **à chaque fois** quelques éléments de base : importer les fonctions de bibliothèque `PIL`, ouvrir une image, faire les modifs, enregistrer/afficher le résultat.

J'affiche là l'image qu'on va utiliser (uploadée dans un sous-dossier images) réduite à 20% :
![Chateau Chaumont](chateau-chaumont.jpg){ width=20% }

Let's go, on va la traficoter dans tous les sens !

## Script 1

???+ question "Compléter ci-dessous"

    {{ IDE('traite-test') }}

<div id="textfield">L'image originale</div>
<div id="original_image"></div>
<div id="textfield">L'image convertie</div>
<div id="new_image"></div>

## Script 2

```python
from PIL import Image
a = "chateau-chaumont.jpg"
im=Image.open(a)

#on créé une fonction qui effectue la conversion de l'image
def tt(image):
    #on récupère les dimensions de l'image
    (c,l)=image.size
    #on créé une image d'arrivée de même dimensions
    imagearrivee=Image.new('RGB',(c,l))
    #on va parcourir toutes les colonnes
    for x in range (c):
        #pour chaque colonne on va parcourir chaque ligne
        for y in range (l):
            #on récupère le triplet RGB de chaque pixel
            pixel=image.getpixel((x,y))
            #traitement de la couleur du pixel
            if pixel[0]>127 or pixel[1]>127 or pixel[2]>127:
                p=(255,255,255)
            else:
                p=(0,0,0)
            #affectation de la couleur du pixel de l'image selon la valeur calculée
            imagearrivee.putpixel((x,y),p)
    #sauvegarde de l'image obtenue
    imagearrivee.save("R01.jpg")
    #affichage de l'image créée
    imagearrivee.show()

#on lance la fonction tt() créée sur notre image
tt(im)

```

## Script 3


